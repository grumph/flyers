import { createStore } from "vuex"

const store = createStore({
  state: {
      image: '',
  },
  mutations: {
      setImage: (state, payload) => (state.image = payload),
  },
  getters: {
      image: state => state.image,
  }
})

export default store
